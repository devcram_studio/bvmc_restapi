package controllers;

import models.Expression;
import models.Parser;
import play.mvc.Controller;
import play.mvc.Result;
import services.HttpPostRequest;
import java.io.IOException;
import views.html.*;


public class ExpressionController extends Controller {

    public Result getExpression(Integer id){
        String query = "select * where {\n" +
                "<http://data.cervantesvirtual.com/expression/"+id+"> ?v ?o}";
        String endPoint = "http://data.cervantesvirtual.com/openrdf-sesame/repositories/data";
        String result = null;
        try {
            result = HttpPostRequest.doRequest(endPoint,query);
        } catch (IOException e) {
            e.printStackTrace();
            result = "ERROR";
        }
        if(request().accepts("text/html")) {
            Expression e = Parser.parseExpression(result);
            return ok(expression.render(e));
        }
        return ok(result);
    }

    public Result getExpressions(Integer work){
        String query = "select * where {\n" +
                "?expression <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdaregistry.info/Elements/c/Expression> . \n" +
                "?expression <http://rdaregistry.info/Elements/e/workExpressed> <http://data.cervantesvirtual.com/work/"+work+">" +
                "}";
        String endPoint = "http://data.cervantesvirtual.com/openrdf-sesame/repositories/data";
        String result;
        try {
            result = HttpPostRequest.doRequest(endPoint,query);
        } catch (IOException e) {
            e.printStackTrace();
            result = "ERROR";
        }
        return ok(result);
    }
}

