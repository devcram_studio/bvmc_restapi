package controllers;

import models.Manifestation;
import models.Parser;
import play.mvc.Controller;
import play.mvc.Result;
import services.HttpPostRequest;
import java.io.IOException;
import views.html.*;

public class ManifestationController extends Controller {

    public Result getManifestation(Integer id){
        String query = "select distinct ?title ?lang ?portada ?author where {\n" +
                "  ?manifestation <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdaregistry.info/Elements/c/Manifestation> .\n" +
                "  ?manifestation <http://purl.org/dc/elements/1.1/identifier> '"+id+"' .\n" +
                "  ?manifestation <http://rdaregistry.info/Elements/m/title> ?title .\n" +
                "  ?manifestation <http://purl.org/dc/elements/1.1/language> ?langId .\n" +
                "  ?langId <http://www.loc.gov/mads/rdf/v1#code> ?lang .\n" +
                "  ?manifestation <http://rdaregistry.info/Elements/m/exemplarOfManifestation> ?item .\n" +
                "  ?item <http://rdaregistry.info/Elements/i/itemSpecificCarrierCharacteristic> 'portada' .\n" +
                "  ?item <http://rdaregistry.info/Elements/i/identifierForTheItem> ?portada .\n" +
                "  ?manifestation <http://rdaregistry.info/Elements/m/workManifested> ?work .\n" +
                "  ?work <http://rdaregistry.info/Elements/w/author> ?authorId .\n" +
                "  ?authorId <http://rdaregistry.info/Elements/a/nameOfThePerson> ?author .\n" +
                "}";
        String endPoint = "http://data.cervantesvirtual.com/openrdf-sesame/repositories/data";
        String result;
        try {
            result = HttpPostRequest.doRequest(endPoint,query);
        } catch (IOException e) {
            e.printStackTrace();
            result = "ERROR";
        }
        if(request().accepts("text/html")) {
            Manifestation m = Parser.parseManifestation(result);
            if(m==null){
                return redirect("/manifestation");
            }
            m.setId(id+"");
            return ok(manifestation.render(m));
        }
        return ok(result);
    }

    public Result getManifestations(Integer expression, Integer work){
        if(expression.equals(0) && work.equals(0))
            return ok(manifestationForm.render());

        String query = "select * where {\n" +
                "?manifestation <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdaregistry.info/Elements/c/Manifestation>";
        if(!expression.equals(0)){
            query += " . ?manifestation <http://rdaregistry.info/Elements/m/expressionManifested> <http://data.cervantesvirtual.com/expression/"+expression+">";
        }
        if(!work.equals(0)){
            query += " . ?manifestation <http://rdaregistry.info/Elements/m/workManifested> <http://data.cervantesvirtual.com/work/"+work+">";
        }
        query += "}";
        String endPoint = "http://data.cervantesvirtual.com/openrdf-sesame/repositories/data";
        String result;
        try {
            result = HttpPostRequest.doRequest(endPoint,query);
        } catch (IOException e) {
            e.printStackTrace();
            result = "ERROR";
        }
        System.out.println(query);
        return ok(result);
    }
}

