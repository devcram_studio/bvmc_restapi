package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.*;
import services.HttpPostRequest;
import java.io.IOException;
import java.util.ArrayList;


public class QueryController extends Controller {

    public Result query(String author, String year, String lang,String title, int offset){

        if(author.equals("") && year.equals("") && lang.equals("") && title.equals(""))
            return ok("error");

        String query = "SELECT ?manifestationId ?manifestationTitle ?portada WHERE{"+
                "?manifestation <http://purl.org/dc/elements/1.1/identifier> ?manifestationId . " +
                "?manifestation <http://rdaregistry.info/Elements/m/title> ?manifestationTitle . "+
                "?manifestation <http://rdaregistry.info/Elements/m/exemplarOfManifestation> ?item . "+
                "?item <http://rdaregistry.info/Elements/i/itemSpecificCarrierCharacteristic> 'portada' . "+
                "?item <http://rdaregistry.info/Elements/i/identifierForTheItem> ?portada";

        if(!year.equals("")){
            query += " . ?manifestation <http://rdaregistry.info/Elements/m/dateOfPublication> <http://data.cervantesvirtual.com/date/"+year+">";
        }

        if(!author.equals("")){
            query += ". ?manifestation <http://rdaregistry.info/Elements/m/workManifested> ?obra" +
                    " . ?author <http://rdaregistry.info/Elements/a/authorOf> ?obra . " +
                    "?author <http://rdaregistry.info/Elements/a/nameOfThePerson> ?authorName . ";
            ArrayList<String> authors = getAuthorsFromDbpedia(author);
            for(int i=0; i<authors.size();i++){
                query+="{?author <http://www.w3.org/2002/07/owl#sameAs> <"+authors.get(i)+">}";
                if(i<authors.size()-1){
                    query+=" UNION ";
                }
            }
        }

        if(!lang.equals("")){
            query += " . ?manifestation <http://purl.org/dc/elements/1.1/language> ?lang";
            query += " . ?lang <http://www.w3.org/2000/01/rdf-schema#label> ?langString";
            query += " FILTER regex(?langString,'"+lang+"','i')";
        }
        query += "}limit 20 offset "+offset;
        String result;

        try {
            String endPoint = "http://data.cervantesvirtual.com/openrdf-sesame/repositories/data";
            System.out.println(query);
            result = HttpPostRequest.doRequest(endPoint,query);
        } catch (IOException e) {
            e.printStackTrace();
            result = "ERROR";
        }
        return ok(result);
    }

    private ArrayList<String> getAuthorsFromDbpedia(String author){
       // String bvmcAuthorsquery = "select * where { ?person <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdaregistry.info/Elements/c/Person>}";
        String endPoint = "http://dbpedia.org/sparql";
        String result;
        ArrayList<String> authors = new ArrayList<>();
        try {
            //String bvmcAuthorsResult = HttpPostRequest.doRequest("http://data.cervantesvirtual.com/openrdf-sesame/repositories/data", bvmcAuthorsquery);
            //JsonNode bvmcAuthors = Json.parse(bvmcAuthorsResult).get("results").get("bindings");

            String query = "select distinct ?author where {\n" +
                    "?author <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> . ";


           /* for(int i=0;i<bvmcAuthors.size();i++){
                query += "{?author"(bvmcAuthors.get(i).get("person").get("value").asText());
            }*/

            query += "?author <http://www.w3.org/2000/01/rdf-schema#label> ?label\n" +
                    "FILTER regex(?label,'"+author+"','i')\n" +
                    "}";

            result = HttpPostRequest.doRequest(endPoint, query);
            JsonNode nodes = Json.parse(result).get("results").get("bindings");

            for(int i=0;i<nodes.size();i++){
                authors.add(nodes.get(i).get("author").get("value").asText());
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return authors;
    }
}

