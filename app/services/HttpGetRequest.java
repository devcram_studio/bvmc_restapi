package services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by nico on 21/04/16.
 */
public class HttpGetRequest {

    public static String doRequest(String urlString) throws IOException {

        URL url = new URL(urlString);

        HttpURLConnection conn =
                (HttpURLConnection) url.openConnection();
        conn.addRequestProperty("Accept", "application/sparql-results+json");
        if (conn.getResponseCode() != 200) {

            return conn.getResponseMessage();
        }
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line + '\n');
        }
        rd.close();

        conn.disconnect();
        return sb.toString();
    }
}
