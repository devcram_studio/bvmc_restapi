package models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nico on 24/04/16.
 */
public class Expression {
    private String id,title,label,language,work;
    private List<Manifestation> manifestations;

    public Expression(){
        manifestations = new ArrayList<>();
    }

    public Expression(String id, String title, String label, String language, String work, List<Manifestation> manifestations) {
        this.id = id;
        this.title = title;
        this.label = label;
        this.language = language;
        this.work = work;
        this.manifestations = manifestations;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public List<Manifestation> getManifestations() {
        return manifestations;
    }

    public void setManifestations(List<Manifestation> manifestations) {
        this.manifestations = manifestations;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void addManifestation(Manifestation m){
        manifestations.add(m);
    }
}
