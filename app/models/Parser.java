package models;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;

/**
 * Created by nico on 25/04/16.
 */
public class Parser {

    public static Expression parseExpression(String s){
        JsonNode node = Json.parse(s).get("results").get("bindings");
        Expression e = new Expression();
        for(int i=0; i<node.size();i++){
            String verb = node.get(i).get("v").get("value").asText();
            String object = node.get(i).get("o").get("value").asText();
            if(verb.equals("http://rdvocab.info/Elements/#title")){
                e.setTitle(object);
            }else if(verb.equals("http://www.w3.org/2000/01/rdf-schema#label")){
                e.setLabel(object);
            }else if(verb.equals("http://rdaregistry.info/Elements/e/languageOfExpression")){
                e.setLanguage(object);
            }else if(verb.equals("http://rdaregistry.info/Elements/e/identifierForTheExpression")){
                e.setId(object);
            }else if(verb.equals("http://rdaregistry.info/Elements/e/workExpressed")){
                e.setWork(object);
            }else if(verb.equals("http://rdaregistry.info/Elements/e/manifestationOfExpression")){
                e.addManifestation(new Manifestation(object));
            }
        }
        return e;
    }

    public static Manifestation parseManifestation(String s){
        if(Json.parse(s).get("results").get("bindings").size()==0)
            return null;
        JsonNode node = Json.parse(s).get("results").get("bindings").get(0);

        String title = node.get("title").get("value").asText();
        String portada = node.get("portada").get("value").asText();
        String lang = node.get("lang").get("value").asText();
        String author = node.get("author").get("value").asText();

        Manifestation m = new Manifestation(title,author,portada,lang);

        return m;
    }
}
