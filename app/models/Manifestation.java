package models;

/**
 * Created by nico on 24/04/16.
 */
public class Manifestation {
    private String id;
    private String url;
    private String title;
    private String portada;
    private String language;
    private String author;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Manifestation(){}

    public Manifestation(String s){
        this.url = s;
    }

    public Manifestation(String title, String author,String portada, String language) {
        this.title = title;
        this.author = author;
        this.portada = portada;
        this.language = language;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPortada() {
        return portada;
    }

    public void setPortada(String portada) {
        this.portada = portada;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
