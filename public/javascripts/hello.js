angular.module('BVMC_app', ['angular-loading-bar', 'ngAnimate','infinite-scroll','angular.filter'])
    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeBar = false;
        //cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Loading...</div>';
    }])
    .controller('SearchController', function($scope, $http) {
        var sc = this;
        sc.scrollDisabled = true;
        sc.author = "";
        sc.year = "";
        sc.lang = "";
        sc.title = "";
        sc.books = [];
        sc.query = "";
        sc.offset = 0;

        sc.search = function() {
            sc.books = [];
            sc.scrollDisabled = false;
            sc.offset = 0;
            sc.loadMore();
        };

        sc.loadMore = function() {
            if (sc.scrollDisabled) return;
            sc.scrollDisabled = true;

            sc.query = 'query?author='+sc.author+'&year='+sc.year+'&lang='+sc.lang+'&title='+sc.title+'&offset='+sc.offset;
            $http.get(
                sc.query,
                {headers: { 'Accept': 'application/json' }}
            ).success(function(data) {
                var results = angular.fromJson(data).results.bindings;
                for(var i = 0; i<results.length; i++){
                    sc.books.push(results[i]);
                }
                if(results.length>0){
                    sc.offset += 20;
                    sc.scrollDisabled = false;
                }
            });
        }
    }).controller('ManifestationController', function($window) {
           var mc = this;
           mc.mid = '';

           mc.getManifestation = function() {
               $window.location.href = '/manifestation/' + mc.mid;
           };
        });

