name := """/home/nico/workspace/BDMC_REST"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "org.openrdf.sesame" % "sesame-runtime" % "2.7.9",
  "ch.qos.logback" % "logback-classic" % "1.0.13"
)
